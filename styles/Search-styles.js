import {StyleSheet, Dimensions} from 'react-native';

const styles=StyleSheet.create({
    container:{
        flex: 1,
      },
      mapContainer: {
        height:300
      },
      map: {
        flex: 1,
        width: Dimensions.get("window").width,
        height: Dimensions.get("window").height,
        marginBottom: '0%',
      }
});

export default styles;
