import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Welcome from '../screens/Welcome';
import SignUp from '../screens/SignUp';
import TabNavigation from './TabNavigation';
import SignIn from '../screens/SignIn';
import Resultsearch from '../screens/Resultsearch';
import Profile from '../screens/Profile';
import Resultcar from '../screens/Resultcar';
import License from '../screens/License';
import Summary from '../screens/Summary';
import Cni from '../screens/Cni';
import Rent from '../screens/Rent';
import BottomScrollSheet from '../component/SubmitSearch';
import {Button} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

export type RootStackParamList = {
  Search: undefined;
  Resultsearch: undefined;
  Resultcar: undefined;
  Profile: undefined;
  Welcome: undefined;
  SignUp: undefined;
  SignIn: undefined;
  Resultcar: undefined;
  License: undefined;
  Summary: undefined;
};

const RootStack = createStackNavigator<RootStackParamList>();

const screenOptions = {
  headerShown: false,
};


const Navigation = () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator
        screenOptions={({route}) => {

           const headerName = getFocusedRouteNameFromRoute(route) ?? 'Rechercher';
             switch (headerName) {
             case 'Rechercher':
                    return 'Rechercher';
               case 'Mes locations':
                 return 'Mes locations';
               case 'Nos vehicules':
                 return 'Nos vehicules';
               case 'Profil':
                 return 'Profil';
             }

          return {
            headerLeft: ({onPress, canGoBack}) =>
              canGoBack ? (
                <Button onPress={onPress} transparent>
                  <Icon name="arrowleft" />
                </Button>
              ) : null,
            headerTitle: headerName,
          };
        }}>
        <RootStack.Screen component={TabNavigation} options={screenOptions} name="Search" />
        <RootStack.Screen component={Resultsearch} name="Resultsearch" options={screenOptions} />
        <RootStack.Screen component={Resultcar} name="Resultcar" options={screenOptions} />
        <RootStack.Screen component={Welcome} options={screenOptions} name="Welcome" />
        <RootStack.Screen component={Profile} name="Profile" options={screenOptions}/>
        <RootStack.Screen component={SignUp} name="SignUp"  options={screenOptions}/>
        <RootStack.Screen component={SignIn} name="SignIn" options={screenOptions}/>
        <RootStack.Screen component={Rent} name="Rent" options={screenOptions}/>
        <RootStack.Screen component={License} name="License" options={screenOptions}/>
        <RootStack.Screen component={Summary} name="Summary" options={screenOptions}/>
        <RootStack.Screen component={Cni} name="Cni" options={screenOptions}/>
      </RootStack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
