import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Search from '../screens/Search';
import Resultsearch from '../screens/Resultsearch';
import Resultcar from '../screens/Resultcar';
import Rental from '../screens/Rental';
import Rent from '../screens/Rent';
import Vehicules from '../screens/Vehicules';
import Profile from '../screens/Profile';
import Summary from '../screens/Summary';
import Cni from '../screens/Cni';
import Icon from 'react-native-vector-icons/AntDesign';
import Colors from '../utils/Colors';


export type TabNavigationParamList = {
  Search: undefined;
  Profile: undefined;

};

type TabBarOptions = {
  focused: boolean;
  color: string;
  size: number;
};

const Tab = createBottomTabNavigator<TabNavigationParamList>();

const TabNavigation: React.FC = () => {



  const screenOptions = ({route}: any) => ({
    tabBarIcon: ({focused, color}: TabBarOptions) => {
      let iconName;

      if (route.name === 'Rechercher') {
        iconName = focused ? 'search1' : 'search1';
      } else if (route.name === 'Profil') {
        iconName = focused ? 'user' : 'user';
      } else if (route.name === 'Mes locations') {
        iconName = focused ? 'profile' : 'profile';
      } else if (route.name === 'Nos vehicules') {
        iconName = focused ? 'car' : 'car';
      } else {
        iconName = focused ? 'tags' : 'tags';
      }
      return <Icon name={iconName} color={color} type="AntDesign" size={25} />;
    },
  });


/*  const screenOptions = ({route}: any) => {
   // If the focused route is not found, we need to assume it's the initial screen
   // This can happen during if there hasn't been any navigation inside the screen
   // In our case, it's "Feed" as that's the first screen inside the navigator
   const routeName = getFocusedRouteNameFromRoute(route) ?? 'Rechercher';

    tabBarIcon: ({focused, color}: TabBarOptions) => {

   switch (routeName) {
     case 'Rechercher':
       return <Icon name={'search1'} color={color} type="AntDesign" size={25} />;
     case 'Profil':
       return <Icon name={'user'} color={color} type="AntDesign" size={25} />;
     case 'Mes locations':
       return <Icon name={'profile'} color={'green'} type="AntDesign" size={25} />;
    case 'Nos vehicules':
        return <Icon name={'car'} color={'green'} type="AntDesign" size={25} />;
   }
   }

 } */

  return (
    <Tab.Navigator
      screenOptions={screenOptions}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: Colors.brandPrimary,
        inactiveTintColor: 'white',
        style: {
        backgroundColor: '#5e5e5e'},
      }}>
      <Tab.Screen name="Rechercher" component={Search} />
      <Tab.Screen name="Mes locations" component={Rental} />
      <Tab.Screen name="Nos vehicules" component={Vehicules} />
      <Tab.Screen name="Profil" component={Profile} />
    </Tab.Navigator>
  );
};

export default TabNavigation;
