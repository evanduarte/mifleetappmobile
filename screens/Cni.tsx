import React, {useState, useEffect} from 'react';
import Typography from '../components/Typography';
import {StyleSheet, View , Text, SafeAreaView, ScrollView, StatusBar, Image, Dimensions, TouchableOpacity} from 'react-native';
import {Button} from 'react-native-paper';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import {CommonActions} from '@react-navigation/native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import RestClient from 'react-native-rest-client';
import ImgToBase64 from 'react-native-image-base64';
import {ROUTE_URL} from '../router/IpRoute.tsx';
import storage from '@react-native-firebase/storage';
import * as Progress from 'react-native-progress';

const Cni: React.FC = ({navigation, route}) => {
    const [token, setToken] = useState("")
    const [cni, setCni] = useState("")
    const [user, setUser]= useState([])
    const [fileDataFront, setFileDataFront] = useState("")
    const [fileUriFront, setFileUriFront] = useState("")
    const [fileDataBack, setFileDataBack] = useState("")
    const [fileUriBack, setFileUriBack] = useState("")

    //const [imageFrontUri, setImageFrontUri] = useState("")
    //const [imageBackUri, setImageBackUri] = useState("")


    const [fileUriFrontBase64, setFileUriFrontBase64] = useState("")
    const [fileUriBackBase64, setFileUriBackBase64] = useState("")

      const [uploading, setUploading] = useState(false);
      const [transferred, setTransferred] = useState(0);



     useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            fetch('http://'+ROUTE_URL+':3005/api/login')
                        .then(response => response.json())
                        .then( data => {
                          console.log("data ::::",data),
                           setUser(data),
                           setCni(data.user.cni),
                           setToken(data.user.jti)
                            localStorage.setItem("token",data.user.jti)
                            console.log(localStorage.getItem("token"))
                        })
                        .catch(error => console.log(error));
             });
             return unsubscribe;
        },[navigation])


        const  handleCameraFront = () => {
            let options = {
              mediaType: "photo",
              //cameraType: "back",
              storageOptions: {
                  skipBackup: true,
                  path: 'images',
               },
            };
            launchCamera(options, (response) => {
              console.log('Response = ', response);

              if (response.didCancel) {
                console.log('User cancelled image picker');
              } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
              } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
              } else {
                //const source = { uri: response.uri };
                console.log('response', JSON.stringify(response));

              setFileDataFront(response.assets[0].fileName),
              setFileUriFront(response.assets[0].uri)

                ImgToBase64.getBase64String(response.assets[0].uri)
                                                    .then(base64String => setFileUriFrontBase64(base64String))
                                                    .catch(err => console.log(err));
              }
            });



          }

          const  handleCameraBack = () => {
              let options = {
                mediaType: "photo",
                //cameraType: "back",
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                 },
              };
              launchCamera(options, (response) => {
                console.log('Response = ', response);

                if (response.didCancel) {
                  console.log('User cancelled image picker');
                } else if (response.error) {
                  console.log('ImagePicker Error: ', response.error);
                } else if (response.customButton) {
                  console.log('User tapped custom button: ', response.customButton);
                  alert(response.customButton);
                } else {
                  const source = { uri: response.uri };
                  console.log('response', JSON.stringify(response));

                setFileDataBack(response.assets[0].fileName),
                setFileUriBack(response.assets[0].uri)

                  ImgToBase64.getBase64String(response.assets[0].uri)
                                                    .then(base64String => setFileUriBackBase64(base64String))
                                                    .catch(err => console.log(err));
                }
              });





            }



     const handleCni = async () => {


          const filenameFront = fileUriFront.substring(fileUriFront.lastIndexOf('/') + 1);
          const uploadUriFront = Platform.OS === 'ios' ? fileUriFront.replace('file://', '') : fileUriFront;

           const filenameBack = fileUriBack.substring(fileUriBack.lastIndexOf('/') + 1);
            const uploadUriBack = Platform.OS === 'ios' ? fileUriBack.replace('file://', '') : fileUriBack;

              setUploading(true);
              setTransferred(0);

               const taskFront = storage()
                 .ref(filenameFront)
                 .putFile(uploadUriFront);

              // set progress state
               taskFront.on('state_changed', snapshot => {
                 setTransferred(
                   Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
                 );
               });


               try {
                 await taskFront;
               } catch (e) {
                 console.error(e);
               }
               setUploading(false);


               setUploading(true);
            setTransferred(0);

             const taskBack = storage()
               .ref(filenameBack)
               .putFile(uploadUriBack);

            // set progress state
             taskBack.on('state_changed', snapshot => {
               setTransferred(
                 Math.round(snapshot.bytesTransferred / snapshot.totalBytes) * 10000
               );
             });
             try {
               await taskBack;
             } catch (e) {
               console.error(e);
             }
             setUploading(false);

             //  getURLimage front and back



           //-----------------------------------------------
         // "headers": {
           // "content-type": "application/json",
            //"Authorization": "Basic ZWFzeS1jYXItcmVudEBhcmlhZG5leHQuY29tOlJZNkFweUdRXzJsdA=="
            //"x-rapidapi-host": "ariadnext-swagger-v1.p.rapidapi.com"
            //easy-car-rent@ariadnext.com:RY6ApyGQ_2lt
          //},
          //"auth": "Basic ZWFzeS1jYXItcmVudEBhcmlhZG5leHQuY29tOlJZNkFweUdRXzJsdA==",
          //-----------------------------------------------


         if (cni == "not_verified") {

              fetch("https://api-test.idcheck.io/rest/v0/task/image?asyncMode=false", {
                  method: "POST",
                  headers: {
                      "Content-Type": "application/json",
                      "Accept": "application/json",
                      "Authorization": "Basic ZWFzeS1jYXItcmVudEBhcmlhZG5leHQuY29tOlJZNkFweUdRXzJsdA=="
                  },
                  body: JSON.stringify({
                    "frontImage": fileUriFrontBase64,
                    "backImage": fileUriBackBase64,
                    "faceImageCropped": "false",
                    "rectoImageCropped" : "false",
                    "versoImageCropped" : "false",
                    "signatureImageCropped" : "false",
                    "apiVersion" : "1.1"
                  }),
              })


                .then(response => response.json())
                .then(response => {

                console.log(response)

                })
                .catch(err => {
                  console.error(err);
                });

            } else if(cni == "manual") {


                        fetch('http://'+ROUTE_URL+':3005/verification', {
                              method: 'POST',
                              headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                              },
                              body: JSON.stringify({
                                  identity_type: "cni",
                                  user_id: user.user.id,
                                  status: 'await',
                                  imageFront: filenameFront,
                                  imageBack: filenameBack,
                              })
                            })

                            .then(response => response.json())
                            .then( data => {
                            console.log("data verif",data)


                             navigation.navigate('Summary', {})

                            })

                            .catch(error => console.log(error));


                  }
     }

      const renderFileUriFront = () => {
        if (!fileUriFront == false) {
          return <Image
            source={{ uri: fileUriFront }}
            style={styles.images}
          />
        } else {
          return <Image
            source={require('../assets/license.jpg')}
            style={styles.images}
          />
        }
      };

    const renderFileUriBack = () => {
      if (!fileUriBack == false) {
        return <Image
          source={{ uri: fileUriBack }}
          style={styles.images}
        />
      } else {
        return <Image
          source={require('../assets/licenseback.jpg')}
          style={styles.images}
        />
      }
    };

  return (
   <ScrollView>


     <View>
          <Text style={{textAlign:'center',fontSize:20,paddingBottom:10}} >Vérification de votre CNI</Text>
          <Text style={{textAlign:'center',fontSize:16,paddingBottom:10}} >Image Recto</Text>
          <View style={styles.ImageSections}>
            <View>
              {renderFileUriFront()}
              <Text style={{textAlign:'center'}}>Aperçu</Text>
            </View>
          </View>

         <View style={styles.btnParentSection}>

            <TouchableOpacity onPress={handleCameraFront} style={styles.btnSection}  >
              <Text style={styles.btnText}>Lancer la caméra</Text>
            </TouchableOpacity>

          </View>

           <Text style={{textAlign:'center',fontSize:20,paddingBottom:10}} >------------------------------------</Text>
            <Text style={{textAlign:'center',fontSize:16,paddingBottom:10}} >Image Verso</Text>
            <View style={styles.ImageSections}>
              <View>
                {renderFileUriBack()}
                <Text style={{textAlign:'center'}}>Aperçu</Text>
              </View>
            </View>

           <View style={styles.btnParentSection}>

              <TouchableOpacity onPress={handleCameraBack} style={styles.btnSection}  >
                <Text style={styles.btnText}>Lancer la caméra</Text>
              </TouchableOpacity>

            </View>

          <Button mode="outlined" onPress={handleCni}>
            <Typography>{"Commencer la verification"}</Typography>
          </Button>

     </View>
   </ScrollView>
  );
};

const styles = StyleSheet.create({


  scrollView: {
    backgroundColor: 'white',
  },

  body: {
    backgroundColor: 'white',
    justifyContent: 'center',
    borderColor: 'black',
    borderWidth: 1,
    height: Dimensions.get('screen').height - 20,
    width: Dimensions.get('screen').width
  },
  ImageSections: {
    display: 'flex',
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingVertical: 8,
    justifyContent: 'center'
  },
  images: {
    width: 250,
    height: 150,
    borderColor: 'black',
    borderWidth: 1,
    marginHorizontal: 3
  },
  btnParentSection: {
    alignItems: 'center',
    marginTop:10
  },
  btnSection: {
    width: 225,
    height: 50,
    backgroundColor: '#DCDCDC',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
    marginBottom:10
  },
  btnText: {
    textAlign: 'center',
    color: 'gray',
    fontSize: 14,
    fontWeight:'bold'
  }
});

export default Cni;