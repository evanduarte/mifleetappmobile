import React, {Component, useState, useEffect} from 'react';
import Typography from '../components/Typography';
import {View} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import {CommonActions} from '@react-navigation/native';
import {WebView} from 'react-native-webview'
import {Button} from 'react-native-paper';
import {ROUTE_URL} from '../router/IpRoute.tsx';

const Rental: React.FC = ({navigation}) => {
    const [token, setToken] = useState(localStorage.getItem("token"))
    const [user, setUser]= useState([])
     const [key, setKey]= useState(1)





    const runFirst = `
      window.isNativeApp = true;
      true; // note: this is required, or you'll sometimes get silent failures
    `;

     useEffect(() => {
         const unsubscribe = navigation.addListener('focus', () => {
            setKey(key + 1)

            fetch('http://'+ROUTE_URL+':3005/api/login')
                        .then(response => response.json())
                        .then( data => {
                          console.log("data ::::",data),
                           setUser(data),
                           setToken(data.user.jti),
                            localStorage.setItem("token",data.user.jti)
                            console.log(!localStorage.getItem("token"))

                        })

                        .catch(error => console.log(error));

            });

            return unsubscribe;
        },[navigation, key])


  return (

   <View style={{flex: 1, flexDirection:'column'}}>
     { !token == false  &&
     <View style={{flex: 1, flexDirection:'column'}}>
        <WebView
             key={key}
            source={{ uri: 'http://192.168.0.33:3005/rentals_app' }}
            style={{ marginTop: 0 }}
            injectedJavaScriptBeforeContentLoaded={runFirst}
          />

       </View>
       }

      { !token &&
       <View>
            <Button mode="outlined" onPress={() => navigation.navigate('SignUp', {page: "Mes locations"})}>
              <Typography>{"S'inscrire"}</Typography>
            </Button>
            <Button mode="outlined" onPress={() => navigation.navigate('SignIn', {page: "Mes locations"})}>
              <Typography>{"Se connecter"}</Typography>
            </Button>
        </View>
       }
   </View>
  );
};


export default Rental
