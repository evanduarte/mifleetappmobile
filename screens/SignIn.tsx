import React, {Component , useState} from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import {CommonActions} from '@react-navigation/native';
import Typography from '../components/Typography';
import {SafeAreaView } from 'react-native';
import {TextInput, Button} from 'react-native-paper';
import ScrollableContent from '../components/Content';
import 'localstorage-polyfill';
import jwtDecode, { JwtPayload } from "jwt-decode";
import {ROUTE_URL} from '../router/IpRoute.tsx';


type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'SignIn'>;
};

const back = CommonActions.reset({
  index: 0,
  routes: [{name: 'Home'}],
});


const reset = {
  index: 0,
  routes: [{name: 'Profile'}],
};



const SignIn: React.FC<Props> = ({navigation, route}) => {
    const [user, setUser] = useState({})
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [page, setPage] = useState(route.params.page)

    console.log("profil page",page)
     const handleEmailChange = (text) => {
        console.log(text)
        setEmail(text)
    }

    const handlePasswordChange = (text) => {
        console.log(text)
        setPassword(text)
    }

     const handleUser = (user) => {
        setUser(user)
      }

       const handleSkip = () => {
          navigation.dispatch(CommonActions.goBack(back));
        };

      const handleLogIn = () => {
              //evt.preventDefault()
              console.log("jentre")
              fetch('http://'+ROUTE_URL+':3005/api/login', {
                  method: "POST",
                  headers: {
                      "Content-Type": "application/json",
                      "Accept": "application/json",
                  },
                  body: JSON.stringify({ "user": {
                    "email" : email,
                    "password" : password
                  }}),
              })
              .then(resp => resp.json())
              .then(data => {
                  console.log("data",data.user.jti)
                  localStorage.setItem("token", data.user.jti)
                  handleUser(data.user)
                     console.log(route.params.page)
                   navigation.navigate(page);
              })
              setEmail("")
              setPassword("")

            };

  return (

        <SafeAreaView>
          <Button onPress={handleSkip}>
            <Typography>Retour en arriere</Typography>
          </Button>
          <TextInput placeholder="Email" onChangeText={text => handleEmailChange(text)} keyboardType="email-address" />
          <TextInput placeholder="Password" onChangeText={text => handlePasswordChange(text)} secureTextEntry={true} />
          <Button mode="contained" onPress={handleLogIn}>
            <Typography>Log in</Typography>
          </Button>

        </SafeAreaView>

  );

};

export default SignIn;


  try {
    const data = decode(token);
    // valid token format
  } catch(error) {
    // invalid token format
  }