import React, {Component , useState , useEffect} from 'react';
import Typography from '../components/Typography';
import {RootStackParamList} from '../router/Navigation';
import {StackNavigationProp} from '@react-navigation/stack';
import {WebView} from 'react-native-webview';
import {CommonActions} from '@react-navigation/native';
import {StyleSheet, View , Image,SafeAreaView, ScrollView , TouchableOpacity , Dimensions , Text} from 'react-native';
import {Button} from 'react-native-paper';
import ScrollableContent from '../components/Content';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import CarouselCards from '../components/CarouselCards';
import MapView from "react-native-maps";
import { Marker } from 'react-native-maps';
import {mapStyle} from "../context/mapStyle";
import Geocoder from 'react-native-geocoding';
import moment from 'moment';
import 'moment/locale/fr';  // without this line it didn't work
import {ROUTE_URL} from '../router/IpRoute.tsx';
moment.locale('fr')




export const WINDOW_WIDTH = Dimensions.get('window').width
export const CARD_WIDTH = Math.round(WINDOW_WIDTH * 1)

type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'Rent'>;
};

const back = {
  index: 0,
  routes: [{name: 'ResultCar'}],
};



const Rent: React.FC<Props> = ({navigation, route}) => {
    const { data, carImages , carUser, userAvatar, startDate, endDate, distance } = route.params;

      const [token, setToken] = useState("")
        const [user, setUser]= useState([])

     useEffect(() => {
         const unsubscribe = navigation.addListener('focus', () => {
             fetch('http://'+ROUTE_URL+':3005/api/login')
                         .then(response => response.json())
                         .then( data => {
                           console.log("data ::::",data),
                            setUser(data),
                            setToken(data.user.jti)
                             localStorage.setItem("token",data.user.jti)
                             console.log(localStorage.getItem("token"))
                         })

                         .catch(error => console.log(error));
              });
              return unsubscribe;
         },[navigation])

  const handleSkip = () => {
    navigation.dispatch(CommonActions.goBack(back));
  };




  return (
    <ScrollableContent style={styles.content}>
         <Button style={styles.button} onPress={handleSkip}>
            <Typography>Retour en arriere</Typography>
          </Button>
        <SafeAreaView style={styles.container}>
             <ScrollView>
                <View style={styles.card}>
                    <View style={styles.descriptionCar}>
                          <Image
                              style={styles.carImage}
                              source={{
                                        uri: data.first_image,
                                      }}
                            />
                            <View>
                        <Typography style={styles.title}>{data.name}</Typography>
                        <Typography style={styles.plaque}>{data.plaque}</Typography>
                         </View>
                    </View>
                </View>
                 <View style={styles.card}>
                    <Typography style={styles.h2}>{"Date de location"}</Typography>
                    <Typography style={styles.p}>{"Du "}{(moment(startDate).locale("fr").format('LLL'))}</Typography>
                    <Typography style={styles.p}>{"Au "}{(moment(endDate).locale("fr").format('LLL'))}</Typography>
                </View>
                 <View style={styles.card}>
                    <Typography style={styles.h2}>{"Distance"}</Typography>
                    <Typography style={styles.p}>{distance}{" km"}</Typography>

                </View>
            </ScrollView>
            <Button mode="outlined" color="#841584" style={styles.buttonRent}>
                 <Typography>{"Confirmer la location "}{data.price_cents}{"€"}</Typography>
             </Button>

        </SafeAreaView>

    </ScrollableContent>
  );
};
//



const styles = StyleSheet.create({
  content: {
    justifyContent: 'flex-start',
    flex: 1,
    marginBottom: 80,
    width: CARD_WIDTH

  },
   container: {
         flexDirection: "column",
         width: '100%'
      },
    title: {
        fontSize: 18,
        textAlign: 'center'
    },
    containerCarousel: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height:240
      },
     card: {
        backgroundColor: 'white',
        position: 'relative',
        marginBottom: 20,
        padding: 10
     },
     text: {
        fontSize: 14,
        marginLeft: 5,
        marginBottom:5,
        color: "black"
     },
       mapContainer: {
         flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
       },
       map: {
         flex: 1,
         width: '100%',
         height: 200,

       },
       h2: {
        fontSize: 16,
        marginLeft: 5,
        marginBottom: 5
       },
       p: {
           fontSize: 12,
           marginLeft: 5,
           marginBottom: 5
          },
       carImage: {
         width: 70,
         height: 70,
         borderRadius: 50,
        marginRight:12,
        marginLeft: 5
       },
       userName: {
        fontSize: 14
       },
       descriptionCar: {
            flex:1,
            flexDirection:'row',
            alignItems:'center'
       },
       voir: {
        fontSize: 12,
        fontWeight: "bold"
       },
       caracteristiques: {
        flexDirection:"row",
        marginLeft:5
       },
       buttonRent: {
        borderWidth: 2,
        borderRadius: 3,
       }

});

export default Rent