import React, {Component, useCallback, useMemo, useRef } from 'react';
import Typography from '../components/Typography';
import {Dimensions, View, Text, StyleSheet } from "react-native";
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import {CommonActions} from '@react-navigation/native';
import MapView from "react-native-maps";
import styles from '../styles/Search-styles';
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import { PermissionsAndroid } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import BottomScrollSheet from '../components/BottomScrollSheet';
import { Marker } from 'react-native-maps';
import {mapStyle} from "../context/mapStyle";
import Geocoder from 'react-native-geocoding';

Geocoder.init("AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc", {language : "fr"});





export default class Search extends Component {


  constructor(props) {

    super(props);

    this.state = {
      latitude:  48.828839,
      longitude: 2.378998,
      address: '',
      error: null,
      searchKeyword:'',
      region:null,
      center:null,
      navigation: null
    };
     console.log("this prosps search",this.props)

  }


  componentDidMount() {
    Geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          region: {
            latitude: Number(position.coords.latitude),
            longitude: Number(position.coords.longitude),
            latitudeDelta: 0.2,
            longitudeDelta: 0.2,
          },
        });
        console.log(this.state.region)
        this.setState({latitude: this.state.region['latitude'], longitude: this.state.region['longitude']})
      },
      (error) => this.setState({ error: error.message }),
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
    );
  }

     setStateFromSearch=(value,name) => {
    //this.props.setSearchKeyword(value,name);

      this.setState({ [name]: value} );
      console.log("latitude",this.state.latitude);
    };

    setStateRegionFromSearch=(latitude,longitude) => {
      this.setState({
          region: {
            latitude: latitude,
            longitude: longitude,
            latitudeDelta: 0.1,
            longitudeDelta: 0.1,
          },
    })
  }


    render() {
        return (
            <View style={styles.container}>
                    <View style={styles.mapContainer}>
                        <MapView
                            style={styles.map}
                            region={this.state.region}
                            customMapStyle={mapStyle}
                            followUserLocation={true}
                            zoomEnabled={true}
                            showsUserLocation={false}>
                            <Marker coordinate={{ latitude : this.state.latitude, longitude : this.state.longitude }} image={require('../assets/map-marker.png')} />
                            <Marker coordinate={{ latitude : 48.808839, longitude : 2.388998 }} image={require('../assets/car-marker.png')}/>
                            <Marker coordinate={{ latitude : 48.79839, longitude : 2.368998 }} image={require('../assets/car-marker.png')}/>
                            <Marker coordinate={{ latitude : 48.77839, longitude : 2.358998 }} image={require('../assets/car-marker.png')}/>
                            <Marker coordinate={{ latitude : 48.79839, longitude : 2.358998 }} image={require('../assets/car-marker.png')}/>
                        </MapView>

                    </View>
                    <BottomScrollSheet
                    setStateFromSearch={this.setStateFromSearch}
                    setStateRegionFromSearch={this.setStateRegionFromSearch}
                    latitude={this.state.latitude}
                    longitude={this.state.longitude}
                    navigation={this.props.navigation}
                    />
            </View>
        );


    }
}






