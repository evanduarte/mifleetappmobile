import React, {Component , useState} from 'react';;
import {StyleSheet, View , SafeAreaView} from 'react-native';
import Or from '../components/Or';
import GoogleButton from '../components/GoogleButton';
import Typography from '../components/Typography';
import Center from '../components/Center';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import Spacer from '../components/Spacer';
import {CommonActions} from '@react-navigation/native';
import {Button, TextInput} from 'react-native-paper';
import ScrollableContent from '../components/Content';
import Navigation from './router/Navigation';
import {ROUTE_URL} from '../router/IpRoute.tsx';

type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'SignUp'>;
};

const back = CommonActions.reset({
  index: 0,
  routes: [{name: 'Home'}],
});

const SignUp: React.FC<Props> = ({navigation , route}) => {

        const [email, setEmail] = useState("")
        const [password, setPassword] = useState("")
        const [page, setPage] = useState(route.params.page)

        console.log(page)

     const handleEmailChange = (text) => {
        console.log(text)
        setEmail(text)
    }

    const handlePasswordChange = (text) => {
        console.log(text)
        setPassword(text)
    }

  const handleSkip = () => {
    navigation.dispatch(CommonActions.goBack(back));
  };

    const handleSignUp = () => {

              fetch('http://'+ROUTE_URL+':3005/users', {
                  method: "POST",
                  headers: {
                      "Content-Type": "application/json",
                      "Accept": "application/json",
                  },
                  body: JSON.stringify({ "user": {
                    "email" : email,
                    "password" : password
                  }}),
              })
              .then(resp => resp.json())

                navigation.navigate(page, {page: 'Rechercher'});
            };

  return (
    <SafeAreaView>
          <Button onPress={handleSkip}>
            <Typography>Retour en arriere</Typography>
          </Button>
      <View>
        <TextInput placeholder="Email" keyboardType="email-address" onChangeText={text => handleEmailChange(text)}  />
        <TextInput placeholder="Password" secureTextEntry={true}  onChangeText={text => handlePasswordChange(text)}/>
        <Button mode="contained" onPress={handleSignUp}>
          <Typography>Sign up</Typography>
        </Button>
      </View>
      <Or />
      <GoogleButton />
      <Spacer />
      <Typography variant="caption">
        By signing up you accept the Terms of Service and Privacy Policy.
      </Typography>
      <Center style={styles.center}>
        <Typography>Already have an account?</Typography>
        <Button>
          <Typography>Log in</Typography>
        </Button>
      </Center>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  center: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default SignUp;
