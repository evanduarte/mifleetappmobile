import React, {useState, useEffect} from 'react';
import Typography from '../components/Typography';
import {View , Text} from 'react-native';
import {Button} from 'react-native-paper';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import {CommonActions} from '@react-navigation/native';
import SignIn from '../screens/SignIn';
import SignUp from '../screens/SignUp';
import License from '../screens/License';
import {ROUTE_URL} from '../router/IpRoute.tsx';


const Profile: React.FC = ({navigation, route}) => {
    const [token, setToken] = useState("")
    const [license, setLicense] = useState("")
    const [cni, setCni] = useState("")
    const [user, setUser]= useState([])
    const [pageSignIn, setPageSignIn] = useState()
    const [pageSignUp, setPageSignUp] = useState()
    const [pageLicense, setPageLicense] = useState()
    const [pageCni, setPageCni] = useState()


     useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            fetch('http://'+ROUTE_URL+':3005/api/login')
                        .then(response => response.json())
                        .then( data => {
                          console.log("data ::::",data),
                           setUser(data),
                           setLicense(data.user.license),
                           setCni(data.user.cni),
                           setToken(data.user.jti)
                            localStorage.setItem("token",data.user.jti)
                            console.log(localStorage.getItem("token"))
                        })
                        .catch(error => console.log(error));
                    if (!route.params == false) {
                      console.log('jentre')
                      setPageSignIn(route.params.page);
                      setPageSignUp(route.params.page);
                      setPageLicense(route.params.page);
                      setPageCni(route.params.page);
                      } else {
                          console.log("enter")
                       setPageSignIn('Rechercher');
                       setPageSignUp('SignIn');
                      }
             });
             return unsubscribe;
        },[navigation])

         const handleLogout = () => {
           //evt.preventDefault()
           console.log("jentre")
           fetch('http://'+ROUTE_URL+':3005/api/logout', {
               method: "DELETE",
               headers: {
                   "Content-Type": "application/json"
               },
           })
           .then(() => (
           setToken(""),
           console.log(license),
           localStorage.setItem("token",null)
           ))
         };

  return (
   <View>
    { !token  &&
     <View>
          <Button mode="outlined" onPress={() => navigation.navigate('SignUp', {page: pageSignUp})}>
            <Typography>{"S'inscrire"}</Typography>
          </Button>
          <Button mode="outlined" onPress={() => navigation.navigate('SignIn',{page: pageSignIn})}>
            <Typography>{"Se connecter"}</Typography>
          </Button>
      </View>
     }
     { !token == false  &&
         <View>
           <Typography>{"Vous etes connecté"}</Typography>
           { license == "not_verified" &&
            <Button mode="outlined" onPress={() => navigation.navigate('License', {page: pageLicense})}>
               <Typography>{"Verifier son permis de conduire"}</Typography>
            </Button>
           }
           { license == "manual" &&
               <Button mode="outlined" onPress={() => navigation.navigate('License', {page: pageLicense})}>
                  <Typography>{"Verifier son permis de conduire"}</Typography>
               </Button>
              }

             { cni == "not_verified" &&
             <Button mode="outlined" onPress={() => navigation.navigate('Cni', {page: pageCni})}>
                <Typography>{"Verifier sa piece d'identité"}</Typography>
             </Button>
            }
            { cni == "manual" &&
                <Button mode="outlined" onPress={() => navigation.navigate('Cni', {page: pageCni})}>
                   <Typography>{"Verifier sa piece d'identité"}</Typography>
                </Button>
               }


           <Button mode="outlined" onPress={handleLogout}>
               <Typography>{"Se déconnecter"}</Typography>
            </Button>
         </View>
    }
     </View>
  );
};
export default Profile;