import React, {useLayoutEffect , useState , useEffect } from 'react';
import {StyleSheet, View , Image,SafeAreaView, ScrollView , TouchableOpacity , Text } from 'react-native';
import WelcomeCarousel from '../components/WelcomeCarousel';
import {StackNavigationProp} from '@react-navigation/stack';
import {RootStackParamList} from '../router/Navigation';
import {CommonActions} from '@react-navigation/native';
import Typography from '../components/Typography';
import {Button} from 'react-native-paper';
import ScrollableContent from '../components/Content';
import Resultcar from '../screens/Resultcar';
import Geocoder from 'react-native-geocoding'
import Geolocation from '@react-native-community/geolocation';

Geocoder.init("AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc", {language : "fr"});


type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'Resultsearch'>;
};

const reset = {
  index: 0,
  routes: [{name: 'Search'}],
};

const Welcome: React.FC<Props> = ({navigation, route}) => {
    const [data, setData] = useState([]);
    const [cityCar, setCityCar] = useState([]);
    const { dataSource, numberOfDays , startDate, endDate, distance} = route.params;
    const TotalCar = dataSource.cars.length




  const handleSkip = () => {
    navigation.dispatch(CommonActions.reset(reset));
  };


  return (
    <ScrollableContent style={styles.content}>
      <Button style={styles.button} onPress={handleSkip}>
        <Typography>Retour en arriere</Typography>
      </Button>
      <Typography style={styles.Totalcar}>{TotalCar}{" résultats"}</Typography>
      <SafeAreaView style={styles.container}>
          <ScrollView
                style={styles.scrollView}>
                  {dataSource.cars.map((car, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        style={styles.card}
                        onPress={()=> navigation.navigate('Resultcar', {data: dataSource.cars[index], carImages: dataSource.result_images[index], carUser: dataSource.car_user[index], userAvatar: dataSource.car_user_avatar[index], startDate: startDate, endDate: endDate, distance: distance})}
                      >
                          <Image
                              style={styles.carImage}
                              source={{
                                        uri: car.first_image,
                                      }}
                            />
                          <Typography style={styles.title}>{car.name}</Typography>
                          <Typography style={styles.title}>{car.city}</Typography>
                          <View  style={styles.price}>
                              <Typography style={{textAlign: "center", fontSize: 16, fontWeight: "bold"}}>{car.price_cents}{"€"}</Typography>
                               {numberOfDays == 1 && <Typography style={{textAlign: "center", fontSize: 12, color: "grey"}}>{"Pour "}{numberOfDays}{" jour"}</Typography>}
                               {numberOfDays > 1 && <Typography style={{textAlign: "center", fontSize: 12, color: "grey"}}>{"Pour "}{numberOfDays}{" jours"}</Typography>}
                           </View>
                      </TouchableOpacity>
                    );
                  })
                  }
          </ScrollView>
      </SafeAreaView>
    </ScrollableContent>
  );
};

const styles = StyleSheet.create({
  content: {
    justifyContent: 'flex-start',
    flex: 1,
    marginBottom: 50
  },
  container: {
      alignItems: 'center',
       flexDirection: "column",
       width: '100%'
    },
  button: {
    marginBottom: 7.5,
    justifyContent: 'center',
  },
  carImage: {
      width: 300,
      height: 150,
    },
   scrollView: {
     marginHorizontal: 0
   },
   card: {
      marginVertical: 10,
      height : 200,
      borderWidth: 1,
       borderRadius: 2
    },
    title: {
        marginLeft: 8,
        marginTop: 2
    },
    price: {
        position: 'absolute',
        right: 5,
        bottom : 5,
        borderWidth: 0,
        borderRadius: 10,
        backgroundColor: "white",
        padding: 10,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
      },
    Totalcar: {
        marginLeft:12
    }
});

export default Welcome;
