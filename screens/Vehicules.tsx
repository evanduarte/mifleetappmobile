import React, {Component, useState, useEffect} from 'react';
import Typography from '../components/Typography';
import {View} from 'react-native';
import {RootStackParamList} from '../router/Navigation';
import {WebView} from 'react-native-webview'
import {ROUTE_URL} from '../router/IpRoute.tsx';





const Vehicules: React.FC = ({navigation}) => {
    const [token, setToken] = useState(localStorage.getItem("token"))
    const [user, setUser]= useState([])
    const [key, setKey]= useState(1)

     const runFirst = `
          window.isNativeApp = true;
          true; // note: this is required, or you'll sometimes get silent failures
        `;


     useEffect(() => {
         const unsubscribe = navigation.addListener('focus', () => {
         setKey(key + 1)
            fetch('http://'+ROUTE_URL+':3005/api/login')
                        .then(response => response.json())
                        .then( data => {
                          console.log("data ::::",data),
                           setUser(data),
                           setToken(data.user.jti),
                            localStorage.setItem("token",data.user.jti)
                            console.log(!localStorage.getItem("token"))
                        })

                        .catch(error => console.log(error));
            console.log('focused');
            });
            return unsubscribe;
        },[navigation,key])


  return (
        <WebView
         key={key}
          source={{ uri: 'http://'+ROUTE_URL+':3005/cars_app' }}
          style={{ marginTop: 0 }}
          injectedJavaScriptBeforeContentLoaded={runFirst}
        />
      );
};

export default Vehicules;
