import React, {Component , useState , useEffect} from 'react';
import Typography from '../components/Typography';
import {RootStackParamList} from '../router/Navigation';
import {StackNavigationProp} from '@react-navigation/stack';
import {WebView} from 'react-native-webview';
import {CommonActions} from '@react-navigation/native';
import {StyleSheet, View , Image,SafeAreaView, ScrollView , TouchableOpacity , Dimensions , Text} from 'react-native';
import {Button} from 'react-native-paper';
import ScrollableContent from '../components/Content';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import CarouselCards from '../components/CarouselCards';
import MapView from "react-native-maps";
import { Marker } from 'react-native-maps';
import {mapStyle} from "../context/mapStyle";
import Geocoder from 'react-native-geocoding';
import Rent from '../screens/Rent';
import {ROUTE_URL} from '../router/IpRoute.tsx';



const apiKey = "AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc"
Geocoder.init("AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc", {language : "fr"});

export const WINDOW_WIDTH = Dimensions.get('window').width
export const CARD_WIDTH = Math.round(WINDOW_WIDTH * 1)

type Props = {
  navigation: StackNavigationProp<RootStackParamList, 'Resultcar'>;
};

const back = {
  index: 0,
  routes: [{name: 'ResultSearch'}],
};



const Resultcar: React.FC<Props> = ({navigation, route}) => {
    const { data, carImages , carUser, userAvatar, startDate, endDate, distance } = route.params;
    const [activeIndex, setActiveIndex] = useState([0]);
    const [descriptionShow, SetDescriptionShow] = useState(false);
      const [token, setToken] = useState("")
        const [user, setUser]= useState([])
    const region = {
                latitude: data.latitude,
                longitude: data.longitude,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
              };

    console.log(startDate, endDate, distance )
     useEffect(() => {
         const unsubscribe = navigation.addListener('focus', () => {
             fetch('http://'+ROUTE_URL+':3005/api/login')
                         .then(response => response.json())
                         .then( data => {
                           console.log("data ::::",data),
                            setUser(data),
                            setToken(data.user.jti)
                             localStorage.setItem("token",data.user.jti)
                             console.log(localStorage.getItem("token"))
                         })

                         .catch(error => console.log(error));
              });
              return unsubscribe;
         },[navigation])

  const handleSkip = () => {
    navigation.dispatch(CommonActions.goBack(back));
  };

  const handleDescription=(e)=>{
          e.preventDefault();

          SetDescriptionShow(true);
          console.log("jentre",descriptionShow)
      }

  const handleRent = () => {
      console.log("rent")
       if (!token) {
         navigation.navigate('Profile', {
            page: 'Resultcar'
          })
       } else {
          navigation.navigate('Rent', {data, carImages , carUser, userAvatar, startDate, endDate, distance })
       }
    };

  return (
    <ScrollableContent style={styles.content}>
         <Button style={styles.button} onPress={handleSkip}>
            <Typography>Retour en arriere</Typography>
          </Button>
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <View style={styles.card}>
                    <Typography style={styles.title}>{data.name}</Typography>
                    <SafeAreaView style={styles.containerCarousel}>
                        <CarouselCards
                        carImages= {carImages}/>
                     </SafeAreaView>
                </View>

                <View style={styles.card}>
                    <Typography style={styles.text}>{"Adresse : "}{data.address}</Typography>

                    <View style={styles.mapContainer}>
                        <MapView
                            pitchEnabled={false}
                            rotateEnabled={false}
                            zoomEnabled={false}
                            scrollEnabled={false}
                            style={styles.map}
                            region={region}
                            customMapStyle={mapStyle}
                            followUserLocation={true}
                            showsUserLocation={false}>
                            <Marker coordinate={{ latitude :data.latitude, longitude : data.longitude }} image={require('../assets/car-marker.png')} />
                        </MapView>
                    </View>
                </View>

                 <View style={styles.card}>
                    <Typography style={styles.h2}>{"Propriétaire du véhicule"}</Typography>
                    <View style={styles.descriptionProprio}>
                        <Image
                          style={styles.avatar}
                          source={{
                                    uri: userAvatar.car_user_avatar,
                                  }}
                        />
                        <Typography style={styles.userName}>{carUser.email}</Typography>
                     </View>
                </View>

                <View style={styles.card}>

                     <Typography style={styles.h2}>{"Description du véhicule"}</Typography>
                     {data.description.length > 150 &&
                          <View>
                             {!descriptionShow && <Typography style={styles.p}>{data.description.slice(0,150)}{" ..."}</Typography>}
                             {!descriptionShow && <Button type="submit" onPress={handleDescription}><Typography style={styles.voir}>{"Voir plus"}</Typography></Button>}
                            {descriptionShow && <Typography style={styles.p}>{data.description}</Typography>}
                         </View>
                     }
                     {data.description.length <= 150 && <Typography style={styles.p}>{data.description}</Typography> }
                </View>
                <View style={styles.card}>
                    <Typography style={styles.h2}>{"Caractéristiques du véhicule"}</Typography>
                    <View style={styles.caracteristiques}>
                        <Text style={{ flex: 1}}>{"Année"}</Text>
                        <Text style={{ flex: 1, textAlign: 'right', marginRight: 12, color: "grey"}}>{data.year}</Text>
                    </View>
                    <View style={styles.caracteristiques}>
                        <Text style={{ flex: 1}}>{"Places"}</Text>
                        <Text style={{ flex: 1, textAlign: 'right', marginRight: 12, color: "grey"}}>{data.places}</Text>
                    </View>
                    <View style={styles.caracteristiques}>
                        <Text style={{ flex: 1}}>{"Boite de vitesse"}</Text>
                        <Text style={{ flex: 1, textAlign: 'right', marginRight: 12, color: "grey"}}>{data.gearbox}</Text>
                    </View>
                    <View style={styles.caracteristiques}>
                        <Text style={{ flex: 1}}>{"Consommation"}</Text>
                        <Text style={{ flex: 1, textAlign: 'right', marginRight: 12, color: "grey"}}>{data.consommation}</Text>
                    </View>

                </View>


            </ScrollView>
            <Button mode="outlined" color="#841584" style={styles.buttonRent} onPress={handleRent}>
                 <Typography>{"Réserver "}{"("}{data.price_cents}{"€"}{")"}</Typography>
             </Button>
        </SafeAreaView>

    </ScrollableContent>
  );
};
//


const styles = StyleSheet.create({
  content: {
    justifyContent: 'flex-start',
    flex: 1,
    marginBottom: 80,
    width: CARD_WIDTH

  },
   container: {
         flexDirection: "column",
         width: '100%'
      },
    title: {
        fontSize: 18,
        textAlign: 'center'
    },
    containerCarousel: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        height:240
      },
     card: {
        backgroundColor: 'white',
        position: 'relative',
        marginBottom: 20,
        padding: 10
     },
     text: {
        fontSize: 14,
        marginLeft: 5,
        marginBottom:5,
        color: "black"
     },
       mapContainer: {
         flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
       },
       map: {
         flex: 1,
         width: '100%',
         height: 200,

       },
       h2: {
        fontSize: 16,
        marginLeft: 5,
        marginBottom: 5
       },
       p: {
           fontSize: 12,
           marginLeft: 5,
           marginBottom: 5
          },
       avatar: {
         width: 50,
         height: 50,
         borderRadius: 50,
        marginRight:12,
        marginLeft: 5
       },
       userName: {
        fontSize: 14
       },
       descriptionProprio: {
            flex:1,
            flexDirection:'row',
            alignItems:'center'
       },
       voir: {
        fontSize: 12,
        fontWeight: "bold"
       },
       caracteristiques: {
        flexDirection:"row",
        marginLeft:5
       },
       buttonRent: {
        borderWidth: 2,
        borderRadius: 3,
       }

});

export default Resultcar;