import React from 'react'
import { View  , StyleSheet} from "react-native"
import Carousel, { Pagination } from 'react-native-snap-carousel'
import CarouselCardItem, { SLIDER_WIDTH, ITEM_WIDTH } from '../components/CarouselCardItem'



const CarouselCards = (carImages) => {
  const [index, setIndex] = React.useState(0)
  const isCarousel = React.useRef(null)


     const data = carImages.carImages

    console.log(data.length)
  return (
    <View style={styles.firstCard}>
      <Carousel
        layout="stack"
        layoutCardOffset={9}
        ref={isCarousel}
        data={data}
        renderItem={CarouselCardItem}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        onSnapToItem={(index) => setIndex(index)}
        useScrollView={true}
      />
    <View
        style={ styles.tabBar }
      >
      <Pagination
        dotsLength={data.length}
        activeDotIndex={index}
        carouselRef={isCarousel}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 0,
          backgroundColor: 'rgba(0, 0, 0, 0.92)',
          marginTop: 0
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
        tappableDots={true}
      />
     </View>

    </View>

  )
}

const styles = StyleSheet.create({
  tabBar: {
    position: 'absolute',
    top: 185
  },
  firstCard: {
    alignItems: 'center'
  }
})

export default CarouselCards