import React, {Component} from 'react';
import { TouchableOpacity} from 'react-native-gesture-handler'
import {
  View,
  Text,
  FlatList,
  TextInput,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import Geocoder from 'react-native-geocoding'
import Geolocation from '@react-native-community/geolocation';
import axios from 'axios';

Geocoder.init("AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc", {language : "fr"});

const API_KEY = 'AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchKeyword: '',
      address:'',
      searchResults: [],
      isShowingResults: false,
      latitude:null,
      longitude:null,
      region:null
    };
  }

componentDidMount() {
    Geolocation.getCurrentPosition(
      (position) => {
        Geocoder.from(position.coords.latitude, position.coords.longitude)
        .then(json => {
          var addressComponent = json.results[0].formatted_address;
          console.log("addresse component",addressComponent);
          this.setState({
          searchKeyword: addressComponent
        });

    console.log("searchKeyword :",this.state.searchKeyword);
    this.handleText(this.state.searchKeyword, 'searchKeyword');


    })

    .catch(error =>
        console.warn(error)
    );

      },
    );
  }

  searchLocation = async (text) => {
    this.setState({searchKeyword: text});
    this.setState({address: text});
    // this.handleText(text,'address');
    // this.handleText(text,'searchKeyword');

    axios
      .request({
        method: 'post',
        url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${API_KEY}&language=fr&components=country%3AFR&input=${this.state.searchKeyword}`,
      })
      .then((response) => {
        this.setState({
          searchResults: response.data.predictions,
          isShowingResults: true,
        });
      })
      .catch((e) => {
      });

  };

  handleText = (text,name) => {
      this.props.setStateFromBottom(text,name);

  }
  geocodeLatLong = (address) => {
    Geocoder.from(address)
                  .then(json => {
                    var latitude = json.results[0].geometry.location.lat;
                    var longitude = json.results[0].geometry.location.lng;
                    this.handleText(latitude,'latitude');
                    this.handleText(longitude,'longitude');
                    this.props.setStateRegionFromBottom(latitude,longitude);
                  })

  }




  render() {
    console.log(this.props.isKeyboadVisible)
    return (

      <SafeAreaView >
        <View style={styles.autocompleteContainer}>
          <TextInput
            placeholder="Rechercher par adresse"
            returnKeyType="search"
            style={styles.searchBox}
            placeholderTextColor="#000"
            onChangeText={(value) => this.searchLocation(value)}

            value={this.state.searchKeyword}
          />
          {this.state.isShowingResults && (
            <FlatList
              data={this.state.searchResults}
              renderItem={({item, index}) => {
                return (
                  <TouchableOpacity
                    style={styles.resultItem}
                    onPress={() => {
                      this.setState({
                        searchKeyword: item.description,
                        isShowingResults: false
                      });
                      this.handleText( item.description,'searchKeyword');
                      this.geocodeLatLong(item.description);
                    }
                    }>
                    <Text>{item.description}</Text>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={(item) => item.id}
              style={styles.searchResultsContainer}
            />
          )}
        </View>
        {this.props.isKeyboadVisible && (
          <View style={styles.dummmy} />
          )}
        {this.state.isShowingResults && (
          <View style={styles.dummmy} />
          )}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    zIndex: 1,
  },
  searchResultsContainer: {
    width: '100%',
    height: 200,
    backgroundColor: '#fff',
    position: 'absolute',
    top: 40,
  },
  resultItem: {
    width: '100%',
    justifyContent: 'center',
    height: 40,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    paddingLeft: 15,
  },


  dummmy: {
    width: 600,
    height: 200,
    backgroundColor: 'white',
    marginTop: 20,
  },
  searchBox: {
    width: '100%',
    height: 40,
    fontSize: 12,
    borderRadius: 8,
    borderColor: '#aaa',
    color: '#000',
    backgroundColor: '#fff',
    borderWidth: 1,
    paddingLeft: 15,
  },
  container: {
    flex: 1,
  },
});

export default App;
