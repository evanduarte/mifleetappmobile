import React, { useCallback, useMemo, useRef, useState , Component} from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, Keyboard } from 'react-native';
import BottomSheet from '@gorhom/bottom-sheet';
import Searchbar from '../components/Searchbar';
import DateTimePicker from '../components/DateTimePicker';
import UserSelectDistance from '../components/UserSelectDistance';
import QuickCategoriesSelect from '../components/QuickCategoriesSelect';
import SubmitSearch from '../components/SubmitSearch';
import { ButtonGroup } from 'react-native-elements';
import Resultsearch from '../screens/Resultsearch';
import moment from 'moment';
import {ROUTE_URL} from '../router/IpRoute.tsx';


  // ref
 // const bottomSheetRef = useRef<BottomSheet>(null);

  // variables
 // const snapPoints = useMemo(() => ['51%','51%'], []);

  // callbacks
 // const handleSheetChanges = useCallback((index: number) => {
 //   console.log('handleSheetChanges', index);
 // }, []);

 // const Separator = () => (
 // <View style={styles.separator} />
//);

 // const userSearch = async values => {
 // const res = await fetch({ url: SERVER_URL, method: "POST", body: values });
 // const data = await res.json();
 // return data;
//};
//const App = () => {




class BottomScrollSheet extends React.Component {


  constructor(props) {

    super(props);

    this.state = {
      latitude: null,
      longitude: null,
      address: '',
      error: null,
      searchKeyword:'',
      bottomSheetRef: null,
      snapPoints: ['50%','50%'],
      region:null,
      isKeyboadVisible: false,
      text: "",
      startDate:new Date(),
      endDate:(new Date()).setHours((new Date()).getHours() + 2),
      distances:null,
      distance:'',
      search_start_date:null,
      search_end_date:null,
      search_start_time:null,
      search_end_time:null,
      isLoading: null,
      dataSource: ''

    };
  }

  userSearch() {
          //console.log(this.state.startDate.format('YYYY MM DD H:mm'));
            console.log("enter in userSearch!!")
          var date_start = (moment(this.state.startDate).format('YYYY-MM-DDTH:mm:ss'));
          date_start = date_start.split("T");
          this.state.search_start_date= date_start[0];
          this.state.search_start_time=date_start[1];

          var date_end = (moment(this.state.endDate).format('YYYY-MM-DDTH:mm:ss'));
          date_end = date_end.split("T");
          this.state.search_end_date= date_end[0];
          this.state.search_end_time=date_end[1];

      console.log("search_end_date",this.state.search_end_date);
      console.log("search_end_time",this.state.search_end_time);
          console.log('http://${ROUTE_URL}:3005/search_json?search_address='+this.state.searchKeyword+'&search_start_date='+this.state.search_start_date+'&search_start_time='+this.state.search_start_time+'&search_end_date='+this.state.search_end_date+'&search_end_time='+this.state.search_end_time+'&search_distance='+this.state.distance+'&commit=Mettre+%C3%A0+jour');

        fetch('http://'+ROUTE_URL+':3005/search_json?search_address='+this.state.searchKeyword+'&search_start_date='+this.state.search_start_date+'&search_start_time='+this.state.search_start_time+'&search_end_date='+this.state.search_end_date+'&search_end_time='+this.state.search_end_time+'&search_distance='+this.state.distance+'&commit=Mettre+%C3%A0+jour')
            .then(response => response.json())
            .then( data => {
              console.log("data ::::",data) ,
              this.setState({
                isLoading: false,
                dataSource: data,
              },
              );
            })

            .catch(error => console.log(error));
  }

 componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      "keyboardDidHide",
      this._keyboardDidHide,
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }




  _keyboardDidShow = () => {
    this.setState({
      isKeyboadVisible: true,
      snapPoints: ['55%','55%']
    });
  };

  _keyboardDidHide = () => {
    this.setState({
      isKeyboadVisible: false,
      snapPoints: ['50%','50%']
    });
  };


    setStateFromBottom=(value,name) => {
    //this.props.setSearchKeyword(value,name);
      this.setState({ [name]: value} );
      this.props.setStateFromSearch(value,name);

    };

    setStateRegionFromBottom=(latitude,longitude) => {
      this.props.setStateRegionFromSearch(latitude,longitude);
    };







  // renders
  render() {
    console.log("c  dataSource",this.state.dataSource, ROUTE_URL)
  return (
     /*  <BottomSheet
        ref={this.state.bottomSheetRef}
        index={1}
        snapPoints={this.state.snapPoints}
      > */
        <View style={styles.contentContainer}>
          <Searchbar
          setStateFromBottom={this.setStateFromBottom}
          setStateRegionFromBottom={this.setStateRegionFromBottom}
          searchKeyword={this.state.searchKeyword}
          latitude={this.state.latitude}
          longitude={this.state.longitude}
          isKeyboadVisible={this.state.isKeyboadVisible}
          address={this.state.address}
          />
          <DateTimePicker
          setStateFromBottom={this.setStateFromBottom}
          startDate={this.state.startDate}
          endDate={this.state.endDate}/>
          <UserSelectDistance
          setStateFromBottom={this.setStateFromBottom}
          distance={this.state.distance} />
          <View style={styles.separator} />
          <QuickCategoriesSelect />
        {!this.state.isKeyboadVisible && (
          <SubmitSearch
           //onPress= { () => this.userSearch() }
           //onPress={ () => console.log('addressh',this.state.searchKeyword,' + ',this.state.startDate, '+', this.state.endDate, '+', this.state.distance )}
          navigation= {this.props.navigation}
          searchKeyword = { this.state.searchKeyword }
          startDate = { this.state.startDate }
          endDate = { this.state.endDate }
          distance = { this.state.distance }
         />
        )}

        </View>

  );
};

}
const styles = StyleSheet.create({

  contentContainer: {
    flex: 1,
    padding: 5,
    paddingVertical: 10,
    backgroundColor:"white",
    borderRadius: 15,
    marginTop:-30

  },
  separator: {

    // marginTop: 130,
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

export default BottomScrollSheet;
