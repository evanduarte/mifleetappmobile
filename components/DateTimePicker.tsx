import React, { useState, Component } from 'react';
import { SafeAreaView, StyleSheet, View, Button, Text } from 'react-native';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import 'moment/locale/fr';  // without this line it didn't work
moment.locale('fr')

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      startDate: new Date(),
        endDate: new Date(),
      //endDate: (new Date()).setHours((new Date()).getHours() + 1),
      shouldShow: null,
      shouldShow2: null,
    };
  }

  setShouldShow = (state) => {
    this.setState({
      shouldShow: state });
  };

  setShouldShow2 = (state) => {
    this.setState({
      shouldShow2: state });
  };

  setstartDate = (startdate) => {
    this.setState({
      startDate: startdate });
    this.props.setStateFromBottom(startdate, "startDate")
  };

  setendDate = (enddate) => {
    this.setState({
      endDate: enddate    });
    this.props.setStateFromBottom(enddate, "endDate")
  };



  // const [startDate, setstartDate] = useState(new Date())
  // const [endDate, setendDate] = useState()
  // const [shouldShow, setShouldShow] = useState(false);
  // const [shouldShow2, setShouldShow2] = useState(false);


render() {



  return (


      <View  >
          <View style={styles.fixToText}>
          <Text style={styles.startTime} >De</Text>
          <Button
          title= {moment(this.state.startDate).format('DD MMM YYYY H:mm')}
          onPress={() => {this.setShouldShow(!this.state.shouldShow);console.log(this.state.shouldShow);this.setShouldShow2(false)}}
          color="#26bf38"       />
          <Text style={styles.startTime}>à</Text>
          <Button
          title= {moment(this.state.endDate).format('DD MMM YYYY H:mm')}
          onPress={() => {this.setShouldShow2(!this.state.shouldShow2);this.setShouldShow(false)}}
          color="#bd009d"          />
          </View>
        {this.state.shouldShow ? (
          <DatePicker
            date={this.state.startDate}
            minimumDate= {new Date()}
            onDateChange={this.setstartDate}
            mode="datetime"
            locale='fr'
            minuteInterval={15}
            />
        ) : null}
        {this.state.shouldShow ? (
                  <Button
          title= "Debut de location"
          color="#26bf38"
          onPress={() => {this.setShouldShow(!this.state.shouldShow);this.setShouldShow2(!this.state.shouldShow2)}}
          />
          ) : null}
        {this.state.shouldShow2 ? (
          <DatePicker
            date={this.state.endDate}
            minimumDate= {this.state.startDate}
            onDateChange={this.setendDate}
            mode="datetime"
            locale='fr'
            minuteInterval={15}
            />
        ) : null}
        {this.state.shouldShow2 ? (
                  <Button
          title= "Fin de location"
          onPress={() => this.setShouldShow2(!this.state.shouldShow2)}
          color="#bd009d"
          />
          ) : null}
      </View>
  )
}
}

const styles = StyleSheet.create({

  fixToText: {
    marginTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 5,
    paddingLeft:5,

  },

  startTime: {
    marginTop: 7,
    paddingLeft:7,
    justifyContent: 'space-around',
  },
});
