import React , { useState, useEffect } from 'react';
import Typography from '../components/Typography';
import {View} from 'react-native';
import {Button} from 'react-native-paper';
import {RootStackParamList} from '../router/Navigation';
import moment from 'moment';
import {ROUTE_URL} from '../router/IpRoute.tsx';



const SubmitSearch: React.FC = ({navigation, searchKeyword , startDate , endDate , distance}) => {
    const [dataSource, setDataSource] = useState([]);
    const [dateBetween, SetDateBetween] = useState([]);
    const oneDay = 24 * 60 * 60 * 1000;
    const userSearch = ()  => {
              //console.log(this.state.startDate.format('YYYY MM DD H:mm'));
                console.log("enter in userSearch")
              var date_start = (moment(startDate).format('YYYY-MM-DDTH:mm:ss'));
              date_start = date_start.split("T");
              var search_start_date = date_start[0];
              var search_start_time = date_start[1];

              var date_end = (moment(endDate).format('YYYY-MM-DDTH:mm:ss'));
              date_end = date_end.split("T");
              var search_end_date = date_end[0];
              var search_end_time = date_end[1];


                SetDateBetween(Math.round(Math.abs((startDate - endDate) / oneDay)))
                if (dateBetween == 0) {
                    SetDateBetween(1)
                }
                console.log("date btw",  dateBetween)


              console.log("url route",'http://'+ROUTE_URL+':3005/search_json?search_address='+searchKeyword+'&search_start_date='+search_start_date+'&search_start_time='+search_start_time+'&search_end_date='+search_end_date+'&search_end_time='+search_end_time+'&search_distance='+distance+'&commit=Mettre+%C3%A0+jour');

            fetch('http://'+ROUTE_URL+':3005/search_json?search_address='+searchKeyword+'&search_start_date='+search_start_date+'&search_start_time='+search_start_time+'&search_end_date='+search_end_date+'&search_end_time='+search_end_time+'&search_distance='+distance+'&commit=Mettre+%C3%A0+jour')
                    .then(response => response.json())
                    .then( data => {
                    // console.log("data ::::",data)
                     //setDataSource(data);

                     navigation.navigate('Resultsearch', {dataSource: data, numberOfDays: dateBetween, startDate: startDate , endDate: endDate , distance: distance})

                    })

                    .catch(error => console.log(error));



      }



  return (
    <View>
      <Button mode="outlined" color="#841584" onPress= {() => userSearch()}  >
        <Typography>Rechercher</Typography>
      </Button>
    </View>
  );
};

export default SubmitSearch;