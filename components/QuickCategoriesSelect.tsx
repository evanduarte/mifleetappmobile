import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableHighlight,
} from 'react-native';


const App = () => {

 var [ isPress, setIsPress ] = React.useState();

  var touchProps = {
    activeOpacity: 1,
    underlayColor: 'red',                               // <-- "backgroundColor" will be always overwritten by "underlayColor"
    style: isPress ? styles.btnPress : styles.btnNormal, // <-- but you can still apply other style changes
    onHideUnderlay: () => setIsPress(false),
    onShowUnderlay: () => setIsPress(true),
    onPress: () => console.log('HELLO'),                 // <-- "onPress" is apparently required
  };


  return (
    <SafeAreaView style={{flex: 2}}>
      <View style={styles.container}>
        <TouchableHighlight {...touchProps}
          style={styles.compacteStyle}
          activeOpacity={0.5}>
          <Image
            source={require('../assets/compacte.png')}
            style={styles.buttonImageIconStyle}
          />
        </TouchableHighlight>
        <TouchableHighlight {...touchProps}
          style={styles.vanStyle}
          activeOpacity={0.5}>
          <Image
            source={require('../assets/van-car.png')}
            style={styles.buttonImageIconStyle}
          />
        </TouchableHighlight>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginLeft: '15%',
  },
  vanStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#aaa',
    height: '100%',
    borderRadius: 5,
    margin: 5,
  },
  compacteStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#aaa',
    height: '100%',
    borderRadius: 5,
    margin: 5,
  },
  buttonImageIconStyle: {
    padding: 10,
    margin: 5,
    height: 80,
    width: 100,
    resizeMode: 'stretch',
  },
  buttonTextStyle: {
    color: '#fff',
    marginBottom: 4,
    marginLeft: 10,
  },
  buttonIconSeparatorStyle: {
    backgroundColor: '#fff',
    width: 1,
    height: 40,
  },
});

export default App;
