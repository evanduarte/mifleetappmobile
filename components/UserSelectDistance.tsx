import React, { useState } from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import DropDownPicker from 'react-native-dropdown-picker';



export default class App extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        open: false,
        distance: null,
        distances: [],
        items: [
                   {label: '50km', value: '50'},
                   {label: '100km', value: '100'},
                   {label: '200km', value: '200'},
                   {label: '300km', value: '300'},
                   {label: '400km', value: '400'},
                   {label: '500km', value: '500'},
                   {label: '600km', value: '600'},
                   {label: '700km', value: '700'},
                   {label: '800km', value: '800'},
               ]

      };

      this.setValue = this.setValue.bind(this);
    }

    setOpen = (open) => {
      this.setState({
        open
      });
    }

    setValue = (callback) => {
      this.setState(state => ({
        value: callback(state.value)
      }));
    }

    setItems = (callback)  => {
      this.setState(state => ({
        items: callback(state.items)
      }));
    }

      changeDistance = (item) => {
            let distance = item;
            let distances;
            this.setState({
                distance,
                distances
            });
            this.props.setStateFromBottom(distance, "distance")
        }


   render() {
    const { open, value, items } = this.state;
     return (
       <View >
            <DropDownPicker
              open={this.state.open}
              value={this.state.value}
              items={this.state.items}
              defaultNull
              setOpen={this.setOpen}
              setValue={this.setValue}
              setItems={this.setItems}
              placeholder="Choisissez votre kilometrage"
              containerStyle={{height: 45, marginTop:5}}
              dropDownStyle={{backgroundColor: '#fafafa', borderWidth:1 }}
              onChangeValue={item => this.changeDistance(item)}
              dropDownMaxHeight={"40%"}

            />
        </View>

      );
    }
  }

