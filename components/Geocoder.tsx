import Geocoder from 'react-native-geocoding';
import Geocode from "react-geocode";

Geocoder.init("AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc", {language : "fr"});

Geocode.setApiKey("AIzaSyDEHC931ZRGi3Ihtl8rrA-WV3HM5jL8HQc");
Geocode.setLanguage("fr");
Geocode.setRegion("fr");
// ROOFTOP, RANGE_INTERPOLATED, GEOMETRIC_CENTER, APPROXIMATE are the accepted values.
Geocode.setLocationType("ROOFTOP");
Geocode.enableDebug();
