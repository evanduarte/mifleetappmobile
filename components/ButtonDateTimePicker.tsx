import React, { useState } from 'react'
import { SafeAreaView, StyleSheet, View, Button } from 'react-native';
import { ButtonGroup } from 'react-native-elements';



export default class ButtonDateTimePicker extends React.Component {
  constructor () {
  super()
  this.state = {
    selectedIndex: 2,
    setShouldShow: false
  }
  this.updateIndex = this.updateIndex.bind(this)
  this.togglebutton = this.togglebutton.bind(this);
}

updateIndex (selectedIndex) {
  this.setState({selectedIndex})
}

  togglebutton() {
        const { open } = this.state;
        this.setState({
            setShouldShow: !setShouldShow,
        });
    }

render () {
  const buttons = ['Date de depart', 'Date de fin']
  const { selectedIndex } = this.state
  const { setShouldShow } = this.state

  return (
    <ButtonGroup
      onPress={this.updateIndex, this.setShouldSho}
      selectedIndex={selectedIndex}
      buttons={buttons}
      containerStyle={{backgroundColor: 'white', width:'100%',marginLeft:0, borderWidth: 1, borderColor: 'black', borderRadius: 5, height:45 }}
    />
    if (setShouldShow) {
      <DatePicker
            date={date}
            minimumDate= {new Date()}
            onDateChange={setDate}
            mode="datetime"
            locale='fr'
            minuteInterval={15}
            />
          } else {
             null
          }
  )
}
}
